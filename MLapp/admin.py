from django.contrib import admin

from .models import Query, Answer, Parser, SingleWord_Query

admin.site.register(Query)
admin.site.register(Answer)
admin.site.register(Parser)
admin.site.register(SingleWord_Query)