import spacy
nlp = spacy.load("en_core_web_sm")

def parse(text):
    TEX = []
    POS = []
    DEP = []    

    for label in nlp(text):
        TEX.append(label.text)
        POS.append(label.pos_)
        DEP.append(label.dep_)
    return TEX,POS,DEP
   
def wordnet(text):
    txt = []
    pos = []
    dep = []
    punc = '''!()-[]{};:'"\, <>./?@#$%^&*_~'''
    
    for i in text: 
        if i in punc: 
            text = text.replace(i, " ") 
    # return text
    for label in nlp(text):
        txt.append(label.text)
        pos.append(label.pos_)
        dep.append(label.dep_)
    return txt, pos, dep
    
    
text = "this is a test.py!!"
wordnet(text)