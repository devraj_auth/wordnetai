from django import forms
from .models import Query, Answer, Parser, SingleWord_Query

class queryForm(forms.ModelForm):
    class Meta:
        model = Query
        fields = '__all__'

    # def __init__(self, *args, **kwargs):
    #     super(queryForm, self).__init__(*args, **kwargs)
    #     self.fields['questionare'].empty_label = 'Select'

class answerForm(forms.ModelForm):
    class Meta:
        model = Answer
        fields = '__all__'

class parserForm(forms.ModelForm):
    class Meta:
        model = Parser
        fields = '__all__'

class queryWordForm(forms.ModelForm):
    class Meta:
        model = SingleWord_Query
        fields = '__all__'





