from django.db import models
#table for question.
class Query(models.Model):
    questions = models.CharField(max_length=200)
    grade = models.CharField(max_length =200, null = False)
    subject = models.CharField(max_length =200, null = False)
    set = models.CharField(max_length =200, null = False)

    def __str__(self):
        return self.questions
#table for answer.
class Answer(models.Model):
    answers = models.CharField(max_length = 200)
    question = models.ForeignKey(Query, on_delete=models.CASCADE)

    def __str__(self):
        return self.answers
#table for parser.
class Parser(models.Model):
    pos_que = models.CharField(max_length =200, null = True)
    pos_ans = models.CharField(max_length =200, null = True)
    dep_que = models.CharField(max_length =200, null = True)
    dep_ans = models.CharField(max_length =200, null = True)
    question = models.ForeignKey(Query, on_delete=models.CASCADE, null=True)
    answer = models.ForeignKey(Answer, on_delete = models.CASCADE, null=True)
    
# table for wordnet
class SingleWord_Query(models.Model):
    word_index = models.IntegerField(null = False)
    pos = models.CharField(max_length= 300, blank = True, null = True)
    dep = models.CharField(max_length = 300, blank = True, null = True)
    synset_id = models.IntegerField(null = False)
    question = models.ForeignKey(Query, on_delete = models.CASCADE)


   
