from django.shortcuts import render, redirect
from .forms import queryForm, answerForm, parserForm, queryWordForm
from .models import Query, Answer, Parser, SingleWord_Query
from django.db import connection
from django.db.models import Sum
from django.forms import modelformset_factory,inlineformset_factory
from MLapp.pos import parse, wordnet


queryset = Query.objects.all()
print(queryset)
#add query view
def add_Query(request):
   
    form1 = queryForm(request.POST or None)
    
    
    if form1.is_valid():
        form1.save()
        
        return redirect('/list')

    context = {
        'form1': form1, 
           
    }
    return render(request, 'add_Query.html', context)



#wordnet view for single word queries.
def wordnetView(request):
    form4 = queryWordForm(request.POST or None)
    if form4.is_valid():
        obj1 = form4.save(commit = False)
        questions = form4.cleaned_data.get('question')
        word_index = form4.cleaned_data.get('word_index')   
        _,pos1,dep1 = wordnet(str(questions))
        obj1.pos = pos1[word_index]
        obj1.dep = dep1[word_index]
        print(pos1)
        print(dep1)
        obj1.save()
        return redirect('/list')
    context = {
        'form4': form4,
    }
    return render(request, 'wordnet.html', context)


# parser view:
def parser(request):
    form3 = parserForm(request.POST or None)
    if form3.is_valid():
        obj = form3.save(commit=False)
        questions = form3.cleaned_data.get('question')
        
        answers = form3.cleaned_data.get('answer')
        # answers = Answer.objects.filter('answers')
        _,pos_que,dep_que = parse(str(questions))
        _,pos_ans,dep_ans = parse(str(answers))
        
        obj.pos_que = pos_que
        obj.dep_que = dep_que
        obj.pos_ans = pos_ans
        obj.dep_ans = dep_ans
        print(questions)
        print(answers)
        obj.save()
        return redirect('/list')
    context = {
        'form3': form3
    }
    return render(request, 'parser.html',context)

def add_Answer(request):
    form2 = answerForm(request.POST or None)
    if form2.is_valid():
            
        form2.save()
        return redirect('/list')

    context = {
        'form2': form2,
    }
    return render(request, 'add_answer.html', context)
def query_List(request):
    parserset = Parser.objects.order_by('-question_id')
    context = {
        "parserset": parserset
    }
    return render(request, 'Qna_List.html', context)

